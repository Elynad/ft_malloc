#include "../incs/ft_malloc.h"

void		*ft_malloc(size_t size)
{
	t_block		*memory_block;

	if (!(glob) || !(glob->is_init))
		glob = init_glob(size);
	else
	{

		if (glob->tiny->block->next)
		{
			ft_putstr("SIZE OF SECOND BLOCK : ");
			ft_putnbr(glob->tiny->block->next->size);
			ft_putchar('\n');
		}
		return((t_block *)search_block(size) + sizeof(t_block));
	}

	ft_putstr("SIZE OF FIRST BLOCK : ");
	ft_putnbr(glob->tiny->block->size);
	ft_putchar('\n');

	/*
		If glob is init ; create a new block ; its memory will begin at
		last block + sizeof(t_block) ; then init it and set prev / next
	*/

	return (&glob->tiny->block + sizeof(t_block));
}

t_block		*search_block(size_t size)
{
	t_block		*tmp;
	t_block		*new_block;

	// Set if (tiny, small, large, etc);
	if (size < 100)
	{
		tmp = glob->tiny->block;
	}
	while (tmp && tmp->next)
		tmp = tmp->next;

	new_block = (t_block *)&tmp->next;
	new_block->size = size;
	new_block->next = NULL;
	new_block->prev = tmp;
	new_block->is_free = 0;

	tmp->next = new_block;

	if (glob->tiny->block->next)
		{
			ft_putstr("SIZE OF SECOND BLOCK : ");
			ft_putnbr(glob->tiny->block->next->size);
			ft_putchar('\n');
		}

	return (new_block);
}

void		print_all_blocks()
{
	t_block		*tmp;

	tmp = glob->tiny->block;

	while (tmp)
	{
		ft_putstr("NEW ITER, size == ");
		ft_putnbr(tmp->size);
		ft_putchar('\n');
		ft_putendl((char *)&tmp + sizeof(t_block));
		tmp = tmp->next;
	}
}

t_glob		*init_glob(size_t size)
{
	glob = (t_glob *)allocate_memory(sizeof(t_glob));
	if (size < 100)
	{
		glob->tiny = (t_page *)allocate_memory(sizeof(t_page) + getpagesize());
		glob->tiny->size = getpagesize();
		glob->tiny->next = NULL;

		glob->tiny->block = (t_block *)&glob->tiny + sizeof(t_page);
		glob->tiny->block->size = size;
		glob->tiny->block->next = NULL;
		glob->tiny->block->prev = NULL;
		glob->tiny->block->is_free = 0;
	}
	else if (size < 1000)
	{
		// Set small page
	}
	else
	{
		// Set large page
	}

	glob->is_init = 1 ;
	
	return (glob);
}

void		*allocate_memory(size_t size)
{
	return (mmap(NULL, sizeof(t_block), PROT_READ | PROT_WRITE,
		MAP_PRIVATE | MAP_ANON, -1, 0));
}
