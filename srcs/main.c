#include "../incs/ft_malloc.h"

int		main(int ac, char **av)
{
	char	*check;
	char	*str;
	char	*str2;
	char	c;
	int		count;

	c = av[2][0];
	count = ft_atoi(av[1]);
	ft_putendl("----------");
	ft_putstr("Will print char ");
	ft_putchar(c);
	ft_putchar(' ');
	ft_putnbr(count);
	ft_putendl(" times.");
	ft_putendl("----------");

	ft_putendl("Allocating memory for check... (real malloc)");
	if (!(check = (char *)malloc(sizeof(char) * count + 1)))
		return (-1);
	ft_putendl("SUCCESS !");
	fill_string(&check, c, count);
	ft_putendl("----------");
	free(check);
	check = NULL;

	ft_putendl("Allocating memory for check...");
	if (!(str = (char *)ft_malloc(sizeof(char) * count + 1)))
		return (-1);
	ft_putendl("SUCCESS !");
	fill_string(&str, c, count);
	ft_putendl("----------");

	ft_putendl("Allocating memory for check...");
	if (!(str2 = (char *)ft_malloc(sizeof(char) * count + 13)))
		return (-1);
	ft_putendl("SUCCESS !");
	fill_string(&str2, 'A', count + 13);
	ft_putendl("----------");


//	free(str);
	str = NULL;

//	print_all_blocks();

	return (0);
}

void	fill_string(char **to_fill, char c, int count)
{
	int		i;

	i = 0;
	while (i < count)
	{
		(*to_fill)[i] = c;
		ft_putchar('[');
		ft_putnbr(i);
		ft_putstr("] = ");
		ft_putchar((*to_fill)[i]);
		ft_putchar('\n');
		i++;
	}
}
