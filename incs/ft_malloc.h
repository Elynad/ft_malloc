#ifndef FT_MALLOC_H

# define FT_MALLOC_H

# include "../libft/libft.h"

# include <sys/mman.h>
# include <sys/resource.h>

/*
**		STRUCTS
*/

typedef struct			s_block
{
	size_t				size;
	char				is_free;
	struct s_block		*next;
	struct s_block		*prev;
}						t_block;

typedef struct			s_page
{
	size_t				size;
	struct s_page		*next;
	t_block				*block;
}						t_page;

typedef struct			s_glob
{
	char				is_init;
	t_page				*tiny;
	t_page				*small;
	t_page				*large;
}						t_glob;

/*
**		GLOBAL STRUCT DEFINED HERE
*/

t_glob					*glob;

/*
**		TESTING (to delete)
*/

void					fill_string(char **to_fill, char c, int count) ;
void					print_all_blocks(void);

/*
**		CORE
*/

void					*ft_malloc(size_t size);
t_glob					*init_glob(size_t size);
void					*allocate_memory(size_t size);
t_block					*search_block(size_t size);

#endif
